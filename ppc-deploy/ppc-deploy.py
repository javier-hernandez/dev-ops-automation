#!/usr/bin/python
import boto3
import sys
import json
import time
import re
import os
sys.path.append(os.path.abspath("./includes"))
from logger import *
from exec_proc import *
tr_target_tag = 'asg-tr-general'
scs_target_tag = 'asg-scs-general-r4.large'
dev_elb_name = "dev-public-epcvip-com-1405428506"
elb_subnets = ["subnet-0718c270", "subnet-14c13a4d", "subnet-6221a507"]
# elb_subnets = ["subnet-13092f76","subnet-d279e68b","subnet-eb1d599c"]
elb_sg = "sg-5dc09638"
# elb_sg = "sg-5c81ea38"
vpc_id = "vpc-63e34106"
# vpc_id = "vpc-09d7866c"
default_region = 'us-west-2'


if len(sys.argv) == 1:
    logger.error("No domain name was specified!")
    sys.exit(1)
else:
    domainname = sys.argv[1]
    domain = domainname.split('.')
if len(sys.argv) == 2:
    logger.error(
        "You forgot to enter in whether or not this domain is PPC or not!")
    sys.exit(1)
elif not(sys.argv[2] == "yes" or sys.argv[2] == "no"):
    print sys.argv[2]
    logger.error(
        "You didn't enter in yes or no to specify if this is a PPC domain or not")
    sys.exit(1)
else:
    ppc = sys.argv[2]

# Validate user entered input


def input_validation():
    logger.info(
        "Please validate that the following information is correct before proceeding\nDomain name = " +
        domainname +
        "\nPPC domain = " +
        ppc +
        "")
    val_input = raw_input("Please enter in yes or no\n")
    if not(sys.argv[2] == "yes" or sys.argv[2] == "no"):
        print sys.argv[2]
        logger.error(
            "You didn't enter in yes or no while validating the information! Please try again!")
        sys.exit(1)
    elif not(val_input == "yes"):
        logger.error(
            "The information provided was not correct. Please try again.")
        sys.exit(1)
    else:
        logger.info("Thanks for validating the information")

# Check if the domain is already registered


def domain_check():
    client = boto3.client('route53domains')
    response = client.check_domain_availability(DomainName=domainname)
    if response["Availability"] == "UNAVAILABLE":
        logger.error("The domain you specified is not available!")
        sys.exit(1)
    else:
        logger.info("The domain " + domainname + " is available")


# Register the domain
def domain_register():
    f = open('json_templates/contact.json')
    contact = f.read()
    contact_json = json.loads(contact)
    logger.info("Registering the domain name " + domainname)
    client = boto3.client('route53domains')
    response = client.register_domain(DomainName=domainname,
                                      DurationInYears=1,
                                      AutoRenew=True,
                                      AdminContact=contact_json,
                                      RegistrantContact=contact_json,
                                      TechContact=contact_json,
                                      PrivacyProtectAdminContact=True,
                                      PrivacyProtectRegistrantContact=True,
                                      PrivacyProtectTechContact=True)
    global operationid
    operationid = response["OperationId"]

# Check on the status of the registered domain


def domain_registration_status():
    waittime = 0
    status = "inprogress"
    dot = "."
    while (status != "SUCCESSFUL"):
        client = boto3.client('route53domains')
        response = client.get_operation_detail(OperationId=operationid)
        status = response["Status"]
        if status != "SUCCESSFUL":
            logger.info(
                "The domain registration status is still in progress" + dot)
            waittime += 1
            if waittime > 30:
                logger.error("The domain registration process took to long")
                sys.exit(1)
            dot = dot + "."
            time.sleep(60)
        else:
            logger.info("The domain registration is complete")

# Request SSL cert


def ssl_cert_order():
    client = boto3.client('acm', region_name=default_region)
    response = client.request_certificate(DomainName=domainname,
                                          ValidationMethod="DNS",
                                          SubjectAlternativeNames=[
                                              '*.' + domainname,
                                          ],)
    logger.info("Requesting the SSL cert")
    global certarn
    certarn = response["CertificateArn"]
    logger.info("The SSL cert ARN is " + certarn)
    logger.info("Waiting for SSL cert request to complete")
    time.sleep(30)
    client = boto3.client('acm', region_name=default_region)
    response = client.describe_certificate(CertificateArn=certarn
                                           )
    cnamename = response["Certificate"]["DomainValidationOptions"][0]["ResourceRecord"]["Name"]
    cnamevalue = response["Certificate"]["DomainValidationOptions"][0]["ResourceRecord"]["Value"]
    client = boto3.client('route53', region_name=default_region)
    response = client.list_hosted_zones_by_name(
        DNSName=domainname,
        MaxItems='1'
    )
    zoneid = response["HostedZones"][0]["Id"]
    logger.info("The Route53 hosted zoneid is " + zoneid)
    f = open('json_templates/cname.json')
    cname_record = f.read()
    cname_json = json.loads(cname_record)
    cname_json["Changes"][0]["ResourceRecordSet"]["Name"] = cnamename
    setid = str(time.time())
    cname_json["Changes"][0]["ResourceRecordSet"]["SetIdentifier"] = setid
    cname_json["Changes"][0]["ResourceRecordSet"]["ResourceRecords"][0]["Value"] = cnamevalue
    client = boto3.client('route53', region_name=default_region)
    response = client.change_resource_record_sets(
        HostedZoneId=zoneid,
        ChangeBatch=cname_json,
    )
    changeid = response["ChangeInfo"]["Id"]
    waittime = 0
    status = "inprogress"
    dot = "."
    while (status != "INSYNC"):
        client = boto3.client('route53', region_name=default_region)
        response = client.get_change(
            Id=changeid
        )
        status = response["ChangeInfo"]["Status"]
        if status != "INSYNC":
            logger.info(
                "The validation CNAME record has been deployed but is not INSYNC yet" + dot)
            waittime += 1
            if waittime > 60:
                logger.error(
                    "The validation CNAME record deployment took too long")
                sys.exit(1)
            dot = dot + "."
            time.sleep(60)
        else:
            logger.info(
                "The validation CNAME record is now INSYNC")
    waittime = 0
    status = "inprogress"
    dot = "."
    while (status != "ISSUED"):
        client = boto3.client('acm', region_name=default_region)
        response = client.describe_certificate(CertificateArn=certarn
                                               )
        status = response["Certificate"]["Status"]
        if status != "ISSUED":
            logger.info(
                "The SSL cert is still in the process of being issued" + dot)
            waittime += 1
            if waittime > 60:
                logger.error(
                    "The SSL cert process took too long")
                sys.exit(1)
            dot = dot + "."
            time.sleep(60)
        else:
            logger.info(
                "The SSL cert for *." +
                domainname +
                " has been issued")

# Deploy the load balancers


def deploy_lb():
    global tr_lb_arn
    global scs_lb_arn
    global ssl_tr_no
    global ssl_scs_no
    global tr_general_name
    tr_general_name = None
    global scs_general_name
    scs_general_name = None
    global tr_listener
    global scs_listener
    # ssl_avail = 1
    ssl_tr_no = 0
    ssl_avail = 1
    ssl_scs_no = 0
    if ppc == "yes":
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_load_balancer(
            Name="tr-ppc-" + domain[0],
            Subnets=elb_subnets,
            SecurityGroups=[
                elb_sg,
            ],
            Scheme='internet-facing',
            Type='application',
            IpAddressType='ipv4'
        )
        tr_lb_arn = response["LoadBalancers"][0]["LoadBalancerArn"]
        logger.info("The ARN for the TR LB is " + tr_lb_arn)
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_load_balancer(
            Name="scs-ppc-" + domain[0],
            Subnets=elb_subnets,
            SecurityGroups=[
                elb_sg,
            ],
            Scheme='internet-facing',
            Type='application',
            IpAddressType='ipv4'
        )
        scs_lb_arn = response["LoadBalancers"][0]["LoadBalancerArn"]
        logger.info("The ARN for the SCS LB is " + scs_lb_arn)
        waittime = 0
        tr_lb_status = "provisioning"
        scs_lb_status = "provisioning"
        dot = "."
        while (tr_lb_status != "active" and scs_lb_status != "active"):
            client = boto3.client('elbv2', region_name=default_region)
            response = client.describe_load_balancers(
                LoadBalancerArns=[
                    tr_lb_arn,
                ]
            )
            tr_lb_status = response["LoadBalancers"][0]["State"]["Code"]
            client = boto3.client('elbv2', region_name=default_region)
            response = client.describe_load_balancers(
                LoadBalancerArns=[
                    scs_lb_arn,
                ]
            )
            scs_lb_status = response["LoadBalancers"][0]["State"]["Code"]
            if (tr_lb_status != "active" and scs_lb_status != "active"):
                logger.info(
                    "The TR and SCS load balancers are still provisioning" + dot)
                waittime += 1
                if waittime > 60:
                    logger.error(
                        "The LB provisioning took too long! Exiting!")
                    sys.exit(1)
                dot = dot + "."
                time.sleep(15)
            else:
                logger.info("The TR and SCS Load Balancers are now active")
    else:
        client = boto3.client('elbv2', region_name=default_region)
        response = client.describe_load_balancers()
        count = 0
        lb_list = []
        for lbname in response["LoadBalancers"]:
            lbmatch = re.compile('tr-general-*')
            if lbmatch.match(response["LoadBalancers"]
                             [count]["LoadBalancerName"]):
                lb_list.append([response["LoadBalancers"][count]["LoadBalancerName"],
                                response["LoadBalancers"][count]["LoadBalancerArn"]])
                lb_temp_arn = response["LoadBalancers"][count]["LoadBalancerArn"]
            count = count + 1
        count = 0
        listener_list = []
        for lb in lb_list:
            client = boto3.client('elbv2', region_name=default_region)
            response2 = client.describe_listeners(
                LoadBalancerArn=lb_list[count][1]
            )
            if(response2['Listeners'][0]['Protocol'] == 'HTTPS'):
                listener1 = response2['Listeners'][0]['ListenerArn']
                lb_list[count].append(listener1)
            if(response2['Listeners'][1]['Protocol'] == 'HTTPS'):
                listener2 = response2['Listeners'][1]['ListenerArn']
                lb_list[count].append(listener2)
            count = count + 1

        count = 0
        ssl_avail = 1
        # ssl_tr_no = 0
        for cert_num in lb_list:
            client = boto3.client('elbv2', region_name=default_region)
            response = client.describe_listener_certificates(
                ListenerArn=cert_num[2],
            )
            if(len(response['Certificates']) < 25):
                if(ssl_avail == 1):
                    logger.info(
                        "The SSL cert for " +
                        domainname +
                        " can be added to the " +
                        lb_list[count][0] +
                        " load balancer")
                    tr_lb_arn = lb_list[count][1]
                    tr_listener = lb_list[count][2]
                    ssl_avail = ssl_avail + 1
                    ssl_tr_no = 1
            count = count + 1

        if(ssl_tr_no == 0):
            lb_len = len(lb_list) - 1
            lb_num = lb_list[lb_len][0].split('-')
            lb_num = (int(lb_num[2]) + 1)
            tr_general_name = "tr-general-" + str(lb_num)
            logger.info(
                'No TR load balancers have space for additional SSL certs so a new LB ' +
                tr_general_name +
                ' will need to be deployed')

        client = boto3.client('elbv2', region_name=default_region)
        response = client.describe_load_balancers()
        count = 0
        lb_list = []
        for lbname in response["LoadBalancers"]:
            lbmatch = re.compile('scs-general-*')
            if lbmatch.match(response["LoadBalancers"]
                             [count]["LoadBalancerName"]):
                lb_list.append([response["LoadBalancers"][count]["LoadBalancerName"],
                                response["LoadBalancers"][count]["LoadBalancerArn"]])
                lb_temp_arn = response["LoadBalancers"][count]["LoadBalancerArn"]
            count = count + 1
        count = 0
        listener_list = []
        for lb in lb_list:
            client = boto3.client('elbv2', region_name=default_region)
            response2 = client.describe_listeners(
                LoadBalancerArn=lb_list[count][1]
            )
            if(response2['Listeners'][0]['Protocol'] == 'HTTPS'):
                listener1 = response2['Listeners'][0]['ListenerArn']
                lb_list[count].append(listener1)
            if(response2['Listeners'][1]['Protocol'] == 'HTTPS'):
                listener2 = response2['Listeners'][1]['ListenerArn']
                lb_list[count].append(listener2)
            count = count + 1

        count = 0
        ssl_avail = 1
        for cert_num in lb_list:
            client = boto3.client('elbv2', region_name=default_region)
            response = client.describe_listener_certificates(
                ListenerArn=cert_num[2],
            )
            if(len(response['Certificates']) < 25):
                if(ssl_avail == 1):
                    logger.info(
                        "The SSL cert for " +
                        domainname +
                        " can be added to the " +
                        lb_list[count][0] +
                        " load balancer")
                    scs_lb_arn = lb_list[count][1]
                    scs_listener = lb_list[count][2]
                    ssl_avail = ssl_avail + 1
                    ssl_scs_no = 1
            count = count + 1
        if(ssl_scs_no == 0):
            lb_len = len(lb_list) - 1
            lb_num = lb_list[lb_len][0].split('-')
            lb_num = (int(lb_num[2]) + 1)
            scs_general_name = "scs-general-" + str(lb_num)
            logger.info(
                'No SCS load balancers have space for additional SSL certs so a new LB ' +
                scs_general_name +
                ' will need to be deployed')

        if(ssl_tr_no == 0):
            client = boto3.client('elbv2', region_name=default_region)
            response = client.create_load_balancer(
                Name=tr_general_name,
                Subnets=elb_subnets,
                SecurityGroups=[
                    elb_sg,
                ],
                Scheme='internet-facing',
                Type='application',
                IpAddressType='ipv4'
            )
            tr_lb_arn = response["LoadBalancers"][0]["LoadBalancerArn"]
            logger.info("The ARN for the TR LB is " + tr_lb_arn)
            waittime = 0
            tr_lb_status = "provisioning"
            dot = "."
            while (tr_lb_status != "active"):
                client = boto3.client('elbv2', region_name=default_region)
                response = client.describe_load_balancers(
                    LoadBalancerArns=[
                        tr_lb_arn,
                    ]
                )
                tr_lb_status = response["LoadBalancers"][0]["State"]["Code"]
                if (tr_lb_status != "active"):
                    logger.info(
                        "The " +
                        tr_general_name +
                        " load balancer is still provisioning" +
                        dot)
                    waittime += 1
                    if waittime > 60:
                        logger.error(
                            "The LB provisioning took too long! Exiting!")
                        sys.exit(1)
                    dot = dot + "."
                    time.sleep(15)
                else:
                    logger.info(
                        "The " +
                        tr_general_name +
                        " load balancer is now active")

            if(ssl_scs_no == 0):
                client = boto3.client('elbv2', region_name=default_region)
                response = client.create_load_balancer(
                    Name=scs_general_name,
                    Subnets=elb_subnets,
                    SecurityGroups=[
                        elb_sg,
                    ],
                    Scheme='internet-facing',
                    Type='application',
                    IpAddressType='ipv4'
                )
                scs_lb_arn = response["LoadBalancers"][0]["LoadBalancerArn"]
                logger.info("The ARN for the SCS LB is " + scs_lb_arn)
                waittime = 0
                scs_lb_status = "provisioning"
                dot = "."
                while (scs_lb_status != "active"):
                    client = boto3.client('elbv2', region_name=default_region)
                    response = client.describe_load_balancers(
                        LoadBalancerArns=[
                            scs_lb_arn,
                        ]
                    )
                    scs_lb_status = response["LoadBalancers"][0]["State"]["Code"]
                    if (scs_lb_status != "active"):
                        logger.info(
                            "The " +
                            scs_general_name +
                            " load balancer is still provisioning" +
                            dot)
                        waittime += 1
                        if waittime > 60:
                            logger.error(
                                "The LB provisioning took too long! Exiting!")
                            sys.exit(1)
                        dot = dot + "."
                        time.sleep(15)
                    else:
                        logger.info(
                            "The " +
                            scs_general_name +
                            " load balancer is now active")


# Deploy TR target
def deploy_tr_target():
    global tr_target_group_arn
    if(ppc == "yes"):
        target_group_name = "tr-ppc-" + domain[0]
    else:
        target_group_name = tr_general_name

    if(ssl_tr_no == 0 or ppc == "yes"):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_target_group(
            Name=target_group_name,
            Protocol='HTTP',
            Port=80,
            VpcId=vpc_id,
            HealthCheckProtocol='HTTP',
            HealthCheckPort='80',
            HealthCheckPath='/hc.php',
            HealthCheckIntervalSeconds=30,
            HealthCheckTimeoutSeconds=5,
            HealthyThresholdCount=5,
            UnhealthyThresholdCount=2,
            TargetType='instance'
        )
        tr_target_group_arn = response["TargetGroups"][0]["TargetGroupArn"]
        logger.info("The TR target group ARN is " + tr_target_group_arn)
        client = boto3.client('ec2', region_name=default_region)
        response = client.describe_instances(
            Filters=[
                {
                    'Name': 'tag:aws:autoscaling:groupName',
                    'Values': [
                        tr_target_tag,
                    ]
                },
            ],
        )
        instance_length = len(response["Reservations"])
        instance_length - 1
        count = 0
        target_instances = ""
        while (count < instance_length):
            instance = response["Reservations"][count]["Instances"][0]["InstanceId"]
            target = dict(Id=instance)
            client = boto3.client('elbv2', region_name=default_region)
            response2 = client.register_targets(
                TargetGroupArn=tr_target_group_arn,
                Targets=[target]
            )
            count = count + 1
            logger.info(
                "Instance ID " +
                instance +
                "  was added to the " +
                target_group_name +
                " target group (" +
                tr_target_group_arn +
                ")")

# Deploy SCS target
def deploy_scs_target():
    global scs_target_group_arn
    if(ppc == "yes"):
        target_group_name = "scs-ppc-" + domain[0]
    else:
        target_group_name = scs_general_name

    if(ssl_scs_no == 0 or ppc == "yes"):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_target_group(
            Name=target_group_name,
            Protocol='HTTP',
            Port=80,
            VpcId=vpc_id,
            HealthCheckProtocol='HTTP',
            HealthCheckPort='80',
            HealthCheckPath='/hc.php',
            HealthCheckIntervalSeconds=30,
            HealthCheckTimeoutSeconds=5,
            HealthyThresholdCount=5,
            UnhealthyThresholdCount=2,
            TargetType='instance'
        )
        global scs_target_group_arn
        scs_target_group_arn = response["TargetGroups"][0]["TargetGroupArn"]
        logger.info("The SCS target group ARN is " + scs_target_group_arn)
        client = boto3.client('ec2', region_name=default_region)
        response = client.describe_instances(
            Filters=[
                {
                    'Name': 'tag:aws:autoscaling:groupName',
                    'Values': [
                            scs_target_tag,
                    ]
                },
            ],
        )
        instance_length = len(response["Reservations"])
        instance_length - 1
        count = 0
        target_instances = ""
        while (count < instance_length):
            instance = response["Reservations"][count]["Instances"][0]["InstanceId"]
            target = dict(Id=instance)
            client = boto3.client('elbv2', region_name=default_region)
            response2 = client.register_targets(
                TargetGroupArn=scs_target_group_arn,
                Targets=[target]
            )
            count = count + 1
            logger.info(
                "Instance ID " +
                instance +
                "  was added to the " +
                target_group_name +
                " target group (" +
                scs_target_group_arn +
                ")")


def add_listeners():
    #certarn = "arn:aws:acm:us-west-2:094491946875:certificate/8c427ab7-adc4-4412-a682-084bf28317aa"
    if(ssl_tr_no == 0 or ppc == "yes"):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_listener(
            LoadBalancerArn=tr_lb_arn,
            Protocol='HTTP',
            Port=80,
            DefaultActions=[
                {
                    'Type': 'forward',
                    'TargetGroupArn': tr_target_group_arn,
                },
            ]
        )
        logger.info("Added HTTP 80 listener to load balancer " + tr_lb_arn)

    if(ssl_scs_no == 0 or ppc == "yes"):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_listener(
            LoadBalancerArn=scs_lb_arn,
            Protocol='HTTP',
            Port=80,
            DefaultActions=[
                {
                    'Type': 'forward',
                    'TargetGroupArn': scs_target_group_arn,
                },
            ]
        )
        logger.info("Added HTTP 80 listener to load balancer " + scs_lb_arn)

    if(ssl_tr_no == 0 or ppc == "yes"):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_listener(
            LoadBalancerArn=tr_lb_arn,
            Protocol='HTTPS',
            Port=443,
            Certificates=[
                {
                    'CertificateArn': certarn,
                },
            ],
            DefaultActions=[
                {
                    'Type': 'forward',
                    'TargetGroupArn': tr_target_group_arn,
                },
            ]
        )
        logger.info("Added HTTPS 443 listener to load balancer " + tr_lb_arn)
        logger.info(
            "Added SSL cert " +
            certarn +
            " to load balancer " +
            tr_lb_arn)

    if(ssl_scs_no == 0 or ppc == "yes"):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.create_listener(
            LoadBalancerArn=scs_lb_arn,
            Protocol='HTTPS',
            Port=443,
            Certificates=[
                {
                    'CertificateArn': certarn,
                },
            ],
            DefaultActions=[
                {
                    'Type': 'forward',
                    'TargetGroupArn': scs_target_group_arn,
                },
            ]
        )
        logger.info("Added HTTPS 443 listener to load balancer " + scs_lb_arn)
        logger.info(
            "Added SSL cert " +
            certarn +
            " to load balancer " +
            scs_lb_arn)

    if(ssl_tr_no == 1):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.add_listener_certificates(
            ListenerArn=tr_listener,
            Certificates=[
                {
                    'CertificateArn': certarn,
                },
            ]
        )
        logger.info("Added the *." + domainname + " SSL cert to the TR  HTTPS listener " + tr_listener)

    if(ssl_scs_no == 1):
        client = boto3.client('elbv2', region_name=default_region)
        response = client.add_listener_certificates(
            ListenerArn=scs_listener,
            Certificates=[
                {
                    'CertificateArn': certarn,
                },
            ]
        )
        logger.info("Added the *." + domainname + " SSL cert to the SCS HTTPS listener " + scs_listener)


def deploy_default_dns():
        # Deploy www CNAME reoord
    client = boto3.client('route53', region_name=default_region)
    response = client.list_hosted_zones_by_name(
        DNSName=domainname,
        MaxItems='1'
    )
    zoneid = response["HostedZones"][0]["Id"]
    logger.info("The Route53 hosted zoneid is " + zoneid)
    f = open('json_templates/cname.json')
    cname_record = f.read()
    cname_json = json.loads(cname_record)
    cname_json["Changes"][0]["ResourceRecordSet"]["Name"] = 'www.' + domainname
    setid = str(time.time())
    cname_json["Changes"][0]["ResourceRecordSet"]["SetIdentifier"] = setid
    cname_json["Changes"][0]["ResourceRecordSet"]["ResourceRecords"][0]["Value"] = domainname
    client = boto3.client('route53', region_name=default_region)
    response = client.change_resource_record_sets(
        HostedZoneId=zoneid,
        ChangeBatch=cname_json,
    )
    logger.info(
        "Added the www." +
        domainname +
        " CNAME record pointing to " +
        domainname)

    # Deploy top level domain A record alias
    client = boto3.client('elbv2', region_name=default_region)
    response = client.describe_load_balancers(
        LoadBalancerArns=[
            scs_lb_arn,
        ],
    )
    scs_lb_dns = response["LoadBalancers"][0]["DNSName"]
    scs_lb_hostedzoneid = response["LoadBalancers"][0]["CanonicalHostedZoneId"]
    f = open('json_templates/arecord_alias.json')
    aname_record = f.read()
    aname_json = json.loads(aname_record)
    aname_json["Changes"][0]["ResourceRecordSet"]["Name"] = domainname
    setid = str(time.time())
    aname_json["Changes"][0]["ResourceRecordSet"]["SetIdentifier"] = setid
    aname_json["Changes"][0]["ResourceRecordSet"]["AliasTarget"]["HostedZoneId"] = scs_lb_hostedzoneid
    aname_json["Changes"][0]["ResourceRecordSet"]["AliasTarget"]["DNSName"] = scs_lb_dns
    client = boto3.client('route53', region_name=default_region)
    response = client.change_resource_record_sets(
        HostedZoneId=zoneid,
        ChangeBatch=aname_json,
    )
    logger.info("Added the A record alias for " + domainname +
                " pointing to the load balancer " + scs_lb_dns)

    # Deploy offer A record alias
    client = boto3.client('elbv2', region_name=default_region)
    response = client.describe_load_balancers(
        LoadBalancerArns=[
            tr_lb_arn,
        ],
    )
    tr_lb_dns = response["LoadBalancers"][0]["DNSName"]
    tr_lb_hostedzoneid = response["LoadBalancers"][0]["CanonicalHostedZoneId"]
    f = open('json_templates/arecord_alias.json')
    aname_record = f.read()
    aname_json = json.loads(aname_record)
    aname_json["Changes"][0]["ResourceRecordSet"]["Name"] = "offer." + domainname
    setid = str(time.time())
    aname_json["Changes"][0]["ResourceRecordSet"]["SetIdentifier"] = setid
    aname_json["Changes"][0]["ResourceRecordSet"]["AliasTarget"]["HostedZoneId"] = tr_lb_hostedzoneid
    aname_json["Changes"][0]["ResourceRecordSet"]["AliasTarget"]["DNSName"] = tr_lb_dns
    client = boto3.client('route53', region_name=default_region)
    response = client.change_resource_record_sets(
        HostedZoneId=zoneid,
        ChangeBatch=aname_json,
    )
    logger.info(
        "Added the A record alias for offer." +
        domainname +
        " pointing to the load balancer " +
        tr_lb_dns)



input_validation();
domain_check();
domain_register();
domain_registration_status();
ssl_cert_order();
deploy_lb()
deploy_tr_target()
deploy_scs_target()
add_listeners()
deploy_default_dns();
