# PPC Deploy

ppc-deploy.py has the following funtionality

1. Checks if a domain is available to register (route53)
2. If the domain is available it registers the domain (route53)
3. Sits in a loop until the domain registration is complete (route53)
4. Once the registration is successful it orders an SSL cert (ACM)
5. Deploys a CNAME record for SSL cert procurement validation (route53)
6. Waits in a loop until the SSL cert validation is complete and the cert is issued (ACM)
7. If PPC domain Deploys load balancer (EC2) ir not PPC domain it will check if there is any available space for additional SSL certs  on the general TR and SCS load balancers and if not it will deploy a new general TR or SCS load balancer
8. If new load balancer(s) are deployed it will Deploy the necessary target group(s) (EC2)
9. If target groups are deployed it will assign the instances to the target group(s) (EC2)
10. If load balancers were deployed it will add  listeners 80 and 443 to load balanceris) (EC2)
11. Assignes the SSL cert to the 443 listener (EC2)
12. Deploys CNAME record for www, A record alias for offer and A record alias for qa

**Requirements**
Requires Python 2.x

**Install the SDK and dependencies**

**Boto SDK Install**
```
pip install boto
```

**Configure the Access Keys**
Create your credentials file at ~/.aws/credentials (C:\Users\USER_NAME\.aws\credentials for Windows users) and save the following lines after replacing the underlined values with your own.
```
[default]
aws_access_key_id = YOUR_ACCESS_KEY_ID
aws_secret_access_key = YOUR_SECRET_ACCESS_KEY
```
