#!/usr/bin/python
import logging
import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('./log/ppc-deploy.log')
handler.setLevel(logging.INFO)

# create console handler
handler2 = logging.StreamHandler()
handler2.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - - %(name)s  - %(levelname)s - [%(filename)s:%(lineno)d] - %(message)s')
handler.setFormatter(formatter)
handler2.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)
logger.addHandler(handler2)

