#!/usr/bin/python
import sys
import subprocess
from subprocess import Popen,PIPE
#System exec function used throughout the script

def sys_command(cmd):
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        stdout, stderr = p.communicate()
        if p.returncode > 0:
                sys.exit(stderr)
        else:
                return stdout
